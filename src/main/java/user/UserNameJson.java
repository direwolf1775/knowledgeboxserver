/**
  * @author Vaclav Hnizda
  *	Knowledge Box
  *	Project 2013-14
  *	SE491-591 - Software Engineering Studio
  */

package user;

import hibernate.User;

import java.util.Date;
import java.util.List;

public class UserNameJson {
	
	private String firstName,lastName;
	
	private Date dateJoined;

	
	private UserNameJson() {  }
	
	public UserNameJson(String firstName, String lastName){
		this.firstName = firstName;
		this.lastName = lastName;
	}
	
	public UserNameJson(String firstName, String lastName, Date dateJoined) {
		this.firstName = firstName;
		this.lastName = lastName;
		this.dateJoined = dateJoined;
	}
	
	public UserNameJson(User userInfo) {
		this.firstName = userInfo.getFirstName();
		this.lastName = userInfo.getLastName();
	}
	
	/**
	 * @return the firstName
	 */
	public String getFirstName() {
		return firstName;
	}

	/**
	 * @param firstName the firstName to set
	 */
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	/**
	 * @return the lastName
	 */
	public String getLastName() {
		return lastName;
	}

	/**
	 * @param lastName the lastName to set
	 */
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	
	/**
	 * Method to return date joined
	 * @return
	 */
	public Date getDateJoined() {
		return dateJoined;
	}
	
	/**
	 * Method to set date joined
	 * @param dateJoined
	 */
	public void setDateJoined(Date dateJoined) {
		this.dateJoined = dateJoined;
	}
	
}
