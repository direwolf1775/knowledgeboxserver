/**
 *  @author Vaclav Hnizda
 *	Knowledge Box
 *	Project 2013-14
 *	SE491-591 - Software Engineering Studio
 */

package user;

import java.util.Date;

public class UserScoreJson {
	private int groupId, numOfQuestions, numOfRightAnswers;
	private String groupTitle;
	private Date dateTaken;
	/**
	 * @return the groupId
	 */
	public int getGroupId() {
		return groupId;
	}
	/**
	 * @param groupId the groupId to set
	 */
	public void setGroupId(int groupId) {
		this.groupId = groupId;
	}
	/**
	 * @return the numOfQuestions
	 */
	public int getNumOfQuestions() {
		return numOfQuestions;
	}
	/**
	 * @param numOfQuestions the numOfQuestions to set
	 */
	public void setNumOfQuestions(int numOfQuestions) {
		this.numOfQuestions = numOfQuestions;
	}
	/**
	 * @return the numOfRightAnswers
	 */
	public int getNumOfRightAnswers() {
		return numOfRightAnswers;
	}
	/**
	 * @param numOfRightAnswers the numOfRightAnswers to set
	 */
	public void setNumOfRightAnswers(int numOfRightAnswers) {
		this.numOfRightAnswers = numOfRightAnswers;
	}
	/**
	 * @return the groupTitle
	 */
	public String getGroupTitle() {
		return groupTitle;
	}
	/**
	 * @param groupTitle the groupTitle to set
	 */
	public void setGroupTitle(String groupTitle) {
		this.groupTitle = groupTitle;
	}
	/**
	 * @return the dateTaken
	 */
	public Date getDateTaken() {
		return dateTaken;
	}
	/**
	 * @param dateTaken the dateTaken to set
	 */
	public void setDateTaken(Date dateTaken) {
		this.dateTaken = dateTaken;
	}
}
