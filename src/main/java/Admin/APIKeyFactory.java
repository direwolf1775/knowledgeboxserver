package Admin;

import java.util.Random;

import hibernate.APIKey;

public class APIKeyFactory {
	private APIKeyFactory() { }
	
	/**
	 * This method will return a new key with a randomized number
	 * @param userId
	 */
	public static APIKey createNewKey(int userId, String companyName)
	{
		Random randomGenerator = new Random();
	      int apiKey = showRandomInteger(10000000,99999999,randomGenerator);
	      APIKey newAPIKey = new APIKey();
	      newAPIKey.setCompanyName(companyName);
	      newAPIKey.setUserId(userId);
	      newAPIKey.setApiKey(apiKey);
		return newAPIKey;
	}
	
	/**
	 * This method can be used to set up a specific API. Primarily for testing
	 * @param userId
	 * @param apiKey
	 * @return
	 */
	public static APIKey createDefinedKey(int userId, int apiKey, String companyName)
	{
		APIKey newAPIKey = new APIKey();
		newAPIKey.setCompanyName(companyName);
		newAPIKey.setUserId(userId);
		newAPIKey.setApiKey(apiKey);
		return newAPIKey;
	}
	
	private static int showRandomInteger(int aStart, int aEnd, Random aRandom){
	    if (aStart > aEnd) {
	      throw new IllegalArgumentException("Start cannot exceed End.");
	    }
	    //get the range, casting to long to avoid overflow problems
	    long range = (long)aEnd - (long)aStart + 1;
	    // compute a fraction of the range, 0 <= frac < range
	    long fraction = (long)(range * aRandom.nextDouble());
	    return ((int)(fraction + aStart));
	  }
}
