/**
 * @author Paul Pelafas
 * @author Anas A. Aljuwaiber
 * Knowledge Box
 * 2014-01-22
 */

package group;

import java.util.Date;

import hibernate.Group;

/**
 * This class is used to prepare class objects for JSON. 
 * These objects are restricted to house only the information that
 * the user needs (removing all extra information)
 */
public class GroupFactory {
	
	/**
	 * Default constructor set to private
	 */
	private GroupFactory(){}
	
	/**
	 * This method is used to set the group status and create an object to be returned
	 * @param groupStatus is the passed in parameter
	 * @return is the return JSON object
	 */
	public static GroupStatusJson validGroup(boolean groupStatus){
		GroupStatusJson output = new GroupStatusJson(groupStatus);
		return output;
	}
	
	/**
	 * This method is used to create a Group object by calling its mutators and returns the object
	 * @param title is the title or Primary Name of the group 
	 * @param description refers to more details about this group
	 * @return is the return object
	 */
	public static Group createGroup(String title, String description){
		Group group = new Group();
		Date date = new Date();
		
		group.setGroupTitle(title);
		group.setGroupDesc(description);
		group.setCreateDate(date);
		
		return group;
	}
	
	/**
	 * This method is used to update the group's title or description
	 * @param group
	 * @param groupTitle
	 * @param groupDesc
	 * @return is the updated group being returned
	 */
	public static Group updateGroup (Group group, String groupTitle, String groupDesc){
		Date modifyDate = new Date();
		
		group.setGroupTitle(groupTitle);
		group.setGroupDesc(groupDesc);
		group.setModifyDate(modifyDate);
	
		return group;
	}
	
	/**
	 * This method is used to create an empty array of GroupInfoJson objects
	 * @param size this is how big you want the array
	 * @return an empty GroupInfoJson[] returned
	 */
	public static GroupDetailsJson[] createGroupDetailsJson(int size){
		
		return new GroupDetailsJson[size];
	}

	/**
	 * This method will take the given information and return it in the form of GroupInfoJson
	 * @param myGroup
	 * @param NumOfQuestions
	 * @param numOfMembers
	 * @return This method returns a GroupDetailsJson object
	 */
	public static GroupDetailsJson createGroupDetailsJson(Group myGroup, int numOfQuestions, int numOfMembers) {

		//Take information
		// Create a new GroupDetailsJson
		// Insert info
		// return new GroupDetailsJson
		
		GroupDetailsJson groupInfo = new GroupDetailsJson();
		
		String title = myGroup.getGroupTitle();
		groupInfo.setGroupTitle(title);
		
		groupInfo.setNumOfQuestions(numOfQuestions);
		groupInfo.setNumOfMembers(numOfMembers);
		
		Date date = myGroup.getCreateDate();
		groupInfo.setCreateDate(date);
		
		groupInfo.setID(myGroup.getId());
		
		return groupInfo;
	}
	
	/**
	 * This method creates a GroupInfoJson
	 * @param myGroup
	 * @return This method returns a GroupInfoJson object
	 */
	public static GroupInfoJson createGroupInfoJson(Group myGroup){
		GroupInfoJson groupInfo = new GroupInfoJson();
		
		String title = myGroup.getGroupTitle();
		groupInfo.setTitle(title);
		
		String desc = myGroup.getGroupDesc();
		groupInfo.setDescription(desc);
		
		return groupInfo;
	}
}
