/**
 * 	@author Anas A. Aljuwaiber
 *  @author Vaclav Hnizda
 *	Knowledge Box
 *	Project 2013-14
 *	SE491-591 - Software Engineering Studio
 */

package main;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicLong;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;


@Controller
public class MainPageController 
{
	
    @RequestMapping(value = "", method=RequestMethod.GET)
   	public @ResponseBody String randomUser() 
    {
    	String admin = "?adminUserName=admin&adminKey=admin";
    	String user_checkId = "?apiKey=7865&email=vaclav@gmail.com&password=Password1";
    	String user_checkId2 = "?apiKey=7865&email=marek@gmail.com&password=Password2";
    	String user_AddNew = "?apiKey=7865&firstName=Vaclav&lastName=Hnizda&email=vaclav@gmail.com&password=Password1";
    	String user_AddNew2 = "?apiKey=7865&firstName=Marek&lastName=Hnizda&email=marek@gmail.com&password=Password2";
    	String user_Update = "?apiKey=7865&firstName=Bob&lastName=Hniz&email=vaclav@gmail.com&password=Password1";
    	String user_DeleteCurrent = "?apiKey=7865&email=vaclav@gmail.com&password=Password1";
    	String group_AddNew = "?apiKey=7865&userId=1&password=Password1&groupName=Algebra&groupDescription=A%20group%20to%20study%20Algebra.";
    	String group_Delete = "?apiKey=7865&userId=1&password=Password1&groupId=1";
    	String group_Details = "?apiKey=7865&userId=1&password=Password1&groupId=1";
    	String group_Update = "?apiKey=7865&userId=1&password=Password1&groupId=1&"
    							+ "&groupTitle=History&groupDescription=American";
    	String id_Pass_Api = "?apiKey=7865&userId=1&password=Password1";
    	String questionMC_Create = "?apiKey=7865&userId=1&password=Password1&groupID=1"
    							+ "&question=What%20does%203.14%20represent."
    							+ "&correctAnswer=The%20math%20fraction%20PI"
    							+ "&answer2=The%20length%20of%20the%20average%20inch%20worm."
    							+ "&answer3=3%20x%201.11"
    							+ "&answer4=The%20reason%20I%20am%20in%20school";
    	String questionTF_Create = "?apiKey=7865&userId=1&password=Password1&groupID=1"
    							+ "&question=The%20moon%20is%20round.&answer=true";
    	String groupListAllQuestions = "?apiKey=7865&groupId=1";
    	String groupListMyQuestions = "?apiKey=7865&userId=1&password=Password1&groupId=1";
    	String group_listAllUsers = "?apiKey=7865&userId=1&password=Password1&groupId=1";
    	String group_inviteUser = "?apiKey=7865&userId=1&password=Password1&groupId=1&invitedEmail=marek@gmail.com";
    	String question_delete = "?apiKey=7865&userId=1&password=Password1&groupID=1&questionId=1";
    		return 	
"<!DOCTYPE html>				"+
"<html>							"+
"<head>							"+
	"<style>						"+
		"body							"+
		"{								"+
		"color:black;					"+
		"background-color:#9DE8AB;		"+
		"}								"+
		"h1								"+
		"{								"+
		"text-align:center;				"+
		"color:#BD85E8;					"+
		"background-color:#D4F257;		"+
		"}								"+
		"h2								"+
		"{								"+
		"text-align:center;				"+
		"color:#BD85E8;					"+
		"background-color:#D4F257;		"+
		"}								"+
		"h3								"+
		"{								"+
		"text-align:center;				"+
		"color:#BD85E8;					"+
		"background-color:#D4F257;		"+
		"}								"+
	"</style>						"+
"</head>						"+

"<body>							"+
	"<h1>							"+

		"<b>KnowledgeBoxes.com API Testing Links</b>" +
		
	"</h1>							"+
	"<h3>							"+
	
	"<br>To manipulate any variable, change values between = and & <br>" +
	"Default values are pre-set in the links"+
	
	"</h3>							"+						
	"<p>							"+
	
		"Check if user #1 exists <a href=" + "/user/login" + user_checkId + ">user/login</a>" +
		"<br><br>"+
		
		"Check if user #2 exists <a href=" + "/user/login" + user_checkId2 + ">user/login</a>" +
		"<br><br>"+
		
    	"Add a new user #1 <a href=" + "/user/create" + user_AddNew + ">user/create</a>"+
    	"<br><br>"+

    	"Add a new user #2 <a href=" + "/user/create" + user_AddNew2 + ">user/create</a>"+
    	"<br><br>"+
		
		"Add a new group for user #1 <a href=" + "/group/create" + group_AddNew + ">group/create</a>"+
		"<br><br>"+
		
		"This is how you invite user #2 to user #1's group <a href=" + "/group/inviteUser" + 
		group_inviteUser + ">/group/inviteUser</a>"+
		"<br><br>"+
		
		"This is how you create a new multiple choice question for group #1 <a href=" + "/question/createMultipleChoice" + 
		questionMC_Create + ">/question/createMultipleChoice</a>"+
		"<br><br>"+
		
		"This is how you create a new True - False question  for group #1<a href=" + "/question/createTrueFalse" + 
		questionTF_Create + ">/question/createTrueFalse</a>"+
		"<br><br>"+
		
		"This is how you request a group #1's description <a href=" + "/group/info" + 
		group_Details + ">/group/info</a>"+
		"<br><br>"+
		
		"This is how you list group #1's members <a href=" + "/group/listUsers" + 
		group_listAllUsers + ">/group/listUsers</a>"+
		"<br><br>"+
		
		"This is how you list all the questions inside group #1 <a href=" + "/group/listAllQuestions" + 
		groupListAllQuestions + ">/group/listAllQuestions</a>"+
		"<br><br>"+ 
		
		"This is how you list all the questions for user #1 they have in group #1 <a href=" + "/group/listMyQuestions" + 
		groupListMyQuestions + ">/group/listMyQuestions</a>"+
		"<br><br>"+ 
		
		"Modify user #1's First and/or Last name <a href=" + "/user/update" + user_Update + ">user/update</a>"+
		"<br><br>"+
		
		"Update an existing group for user #1 <a href=" + "/group/update" + group_Update + ">group/update</a>"+
		"<br><br>"+

		"Delete group #1 from user #1 <a href=" + "/group/delete" + group_Delete + ">group/delete</a>"+
		"<br><br>"+

		"Delete existing user #1 <a href=" + "/user/delete" + user_DeleteCurrent + ">user/delete</a>"+
		"<br><br>"+
		
		"DEMO: List all of a specific user's groups <a href=" + "/user/listGroups" + id_Pass_Api + ">/user/listGroups</a>"+
		"<br><br>"+
		
		"DEMO: List all of a specific user's Scores <a href=" + "/user/listScores" + id_Pass_Api + ">/user/listScores</a>"+
		"<br><br>"+
		
		"List all of a specific user's Questions entered <a href=" + "/user/listQuestions" + id_Pass_Api + ">/user/listQuestions</a>"+
		"<br><br>"+
		
		"Delete a question<a href=" + "/question/delete" + question_delete + "> /question/delete</a>"+
		"<br><br>"+
	
	
		
	"<br></p>						"+
	"<h2>							"+
	
		// ADMIN TOOLS //
		"<b>Admin Use only, this will not be public! </b>" +
		
	"</h2>							"+	
	"<h3>							"+
		
		"If nothing found they return an empty array."+
	
	"</h3>							"+
	"<p>							"+

		"See master list of all users <a href=" + "/admin/listUsers" + admin + ">admin/listUsers</a>"+
		"<br><br>"+
		
		"See customized list of all users <a href=" + "/admin/listAllUsersNames" + admin + ">admin/listAllUsersNames</a>" +
		"<br><br>"+
		
		"See customized list of all user role groups <a href=" + "/admin/listAllUserRoleGroups" + admin + ">admin/listAllUserRoleGroups</a>" +
		"<br><br>"+
		
		"See full list of all Groups <a href=" + "/admin/listGroups" + admin + ">admin/listGroups</a>" +   		
		"<br><br>"+
		
		"See full list of all Questions <a href=" + "/admin/listAllQuestions" + admin + ">admin/listAllQuestions</a>" +   		
		"<br><br>"+
		
		"See full list of all Answers <a href=" + "/admin/listAllAnswers" + admin + ">admin/listAllAnswers</a>"+    		
		"<br><br>"+

		"Set up Sabrina's Website API key <a href=" + "/admin/setSabrinaAPI" + admin + ">admin/admin/setSabrinaAPI</a>"+    		
		
	"</p>							"+
"</body>						"+
"</html>						";
	    	
    }
}