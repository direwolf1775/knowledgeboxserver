/**
 * 	@author Anas A. Aljuwaiber
 *  @author Paul Pelafas
 *	Knowledge Box
 *	Project 2013-14
 *	SE491-591 - Software Engineering Studio
 */

package main;

import java.util.List;

import hibernate.Answer;
import hibernate.Column;
import hibernate.Question;
import hibernate.Table;
import hibernate.UserRoleGroup;
import hibernateLogic.APIKeyCheck;
import hibernateLogic.DatabasePro;
import hibernateLogic.DatabaseTools;
import hibernateLogic.ObjectRetrievalTools;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import question.QuestionDetailsJson;
import question.QuestionFactory;
import question.QuestionStatusJson;
import user.UserFactory;


@Controller
public class QuestionController {
	
	/**
	 * This is the API for creating a Multiple Choice data entry
	 * @param apiKey
	 * @param userId
	 * @param password
	 * @param topicID
	 * @param question
	 * @param correctAnswer
	 * @param answer2
	 * @param answer3
	 * @param answer4
	 * @return returns true if question was entered or false if it failed
	 */
    @RequestMapping(value = "question/createMultipleChoice")
	public @ResponseBody QuestionStatusJson createMCQuestion(
			@RequestParam(value="apiKey", 		required=true) Integer apiKey,
			@RequestParam(value="userId", 			required=true) int userId,
			@RequestParam(value="password", 		required=true) String password,
			@RequestParam(value="groupID", 			required=true) int groupId,
			@RequestParam(value="question", 		required=true) String question,
			@RequestParam(value="correctAnswer", 	required=true) String correctAnswer,
			@RequestParam(value="answer2", 			required=true) String answer2,
			@RequestParam(value="answer3", 			required=true) String answer3,
			@RequestParam(value="answer4", 			required=true) String answer4) throws Exception {
    	 	
    	
    	if (!APIKeyCheck.exists(apiKey)){
    		throw new Exception("Failed to find your API key!");
    	}
    	else {
    		// Set up your Database Tools
    		DatabaseTools myTools = new DatabaseTools();
    		
    		// Start by confirming / finding the user
    		boolean userFound = myTools.dataSearch("User", "id", userId, "password", password);
    		
    		if (!userFound){
    			throw new Exception ("User not found");
    		}
    		
    		else {
    			// next confirm / find the group
    			boolean groupFound = myTools.dataSearch("Group", "id", groupId);
    			
    			if (!groupFound){
    				throw new Exception ("Group not found");
    			}
    			
    			else {
    				
    				//set up db tools to search for a user within a group
    				DatabasePro<UserRoleGroup, Integer, Integer, String> myProTools = new DatabasePro<>();
    				
    				//returns a list searching for a user in a group. size will be either 1 or 0 depending on if they were found
    				List<UserRoleGroup> userRoleGroupList = myProTools.getTypeList(Table.UserRoleGroup, Column.groupId, groupId, Column.userId, userId);
    				
    				//user was not found to be part of the group
    				if (userRoleGroupList.size() == 0){
    					throw new Exception("User is not a part of the group and can not add questions");
    				}
    				
    				//user is a part of the group
    				else {
    					
    					//create question using factory
    					Question questionObj = QuestionFactory.createQuestionMC(userId, groupId, question);
    			    
    					//add question to the database 
    					int newQuestionID = myTools.dataEntry(questionObj);
		    
    					//set up answers
    					Answer correct= QuestionFactory.createAnswer(correctAnswer, 1, newQuestionID, true);
    					Answer wrong1 = QuestionFactory.createAnswer(answer2, 		2, newQuestionID, false);
    					Answer wrong2 = QuestionFactory.createAnswer(answer3, 		3, newQuestionID, false);
    					Answer wrong3 = QuestionFactory.createAnswer(answer4, 		4, newQuestionID, false);

    					// Now adding the answers to the database
    					int correctAnswerId = myTools.dataEntry(correct);

    					int answer2Id = myTools.dataEntry(wrong1);

    					int answer3Id = myTools.dataEntry(wrong2);

    					int answer4Id = myTools.dataEntry(wrong3);

    					// Now check that the question is correctly stored in the database
    					boolean questionFound = myTools.dataSearch("Question", "id", newQuestionID);

    					// Now check that the answers are correctly stored in the database
    					boolean correctAnswerFound = myTools.dataSearch("Answer", "id", correctAnswerId);

    					boolean answer2Found = myTools.dataSearch("Answer", "id", answer2Id);

    					boolean answer3Found = myTools.dataSearch("Answer", "id", answer3Id);

    					boolean answer4Found = myTools.dataSearch("Answer", "id", answer4Id);
   			    
    					// Now check, if not all the database checks are true, return false status
    					if(!(questionFound && correctAnswerFound && answer2Found && answer3Found && answer4Found)) {

    						return new QuestionStatusJson(false);
    					}
    			    
    					return new QuestionStatusJson(true);
    				}
    			}
    		}
    	}
    }

    /**
     * This API is used to create a true/false question for a particular group
     * @param apiKey
     * @param userId
     * @param password
     * @param groupId
     * @param question
     * @param answer
     * @return is the QuestionStatusJson object being returned
     * @throws Exception
     */
    @RequestMapping(value = "question/createTrueFalse")
    public @ResponseBody QuestionStatusJson createTFQuestion(
    		@RequestParam(value="apiKey", 		required=true) Integer apiKey,
    		@RequestParam(value="userId", 			required=true) int userId,
    		@RequestParam(value="password", 		required=true) String password,
    		@RequestParam(value="groupID", 			required=true) int groupId,
    		@RequestParam(value="question", 		required=true) String question,
    		@RequestParam(value="answer", 	required=true) boolean answer) throws Exception
    {

    	if (!APIKeyCheck.exists(apiKey)){
    		
    		throw new Exception("Failed to find your API key!");
    	}
    	
    	else {
    		// Set up your Database Tools
    		DatabaseTools myTools = new DatabaseTools();
    		
    		// Start by confirming / finding the user
    		boolean userFound = myTools.dataSearch("User", "id", userId, "password", password);
    		
    		if (!userFound) {
    			throw new Exception ("User not found");
    		}
    		
    		else {
    			// next confirm the group exists
    			boolean groupFound = myTools.dataSearch("Group", "id", groupId);
    			
    			if (!groupFound){
    				throw new Exception ("Group not found");
    			}
    			
    			//group exists
    			else {
    				
    				//set up db tools to search for a user within a group
    				DatabasePro<UserRoleGroup, Integer, Integer, String> myProTools = new DatabasePro<>();
    				
    				//returns a list searching for a user in a group. size will be either 1 or 0 depending on if they were found
    				List<UserRoleGroup> userRoleGroupList = myProTools.getTypeList(Table.UserRoleGroup, Column.groupId, groupId, Column.userId, userId);
    				
    				//user was not found to be part of the group
    				if (userRoleGroupList.size() == 0){
    					throw new Exception("User is not a part of the group and can not add questions");
    				}
    				
    				//user is part of the group
    				else {
    					
    					 Question questionObj = QuestionFactory.createQuestionTF(userId, groupId, question);
    	    			    
    	    			    //add question to the database 
    	    			    int newQuestionID = myTools.dataEntry(questionObj);

    	    			    // The logic to decide whether the correct answer is "True" or "False"
    	    			    Answer trueAnswer, falseAnswer;
    	    			    
    				    	// If the answer is true then "True" is true. If false, then "True" is false.
    				    	trueAnswer = QuestionFactory.createAnswer("True", 1, newQuestionID, answer);
    	    			    falseAnswer = QuestionFactory.createAnswer("False", 2, newQuestionID, !answer);
    						
    	    			    // Now adding the answers to the database
    	    			    int trueAnswerId = myTools.dataEntry(trueAnswer);
    	    			    int falseAnswerId = myTools.dataEntry(falseAnswer);

    	    			    // Now check that the question is correctly stored in the database
    	    			    boolean questionFound = myTools.dataSearch("Question", "id", newQuestionID);

    	    			    // Now check that the answers are correctly stored in the database
    	    			    boolean trueAnswerFound = myTools.dataSearch("Answer", "id", trueAnswerId);

    	    			    boolean falseAnswerFound = myTools.dataSearch("Answer", "id", falseAnswerId);

    	    			    // Now check, if not all the database checks are true, return false status
    	    			    if(!(questionFound && trueAnswerFound && falseAnswerFound)){
    	    			    	return new QuestionStatusJson(false);
    	    			    }
    	    			    
    	    			    return new QuestionStatusJson(true);   					
    				}    			   
    			}
    		}
    	}    	
    }   

    /**
		This API deletes a question 
     */
    @RequestMapping(value = "question/delete")
    public @ResponseBody QuestionStatusJson delete(
    		@RequestParam(value="apiKey", 		required=true) Integer apiKey,
    		@RequestParam(value="userId", 			required=true) int userId,
    		@RequestParam(value="password", 		required=true) String password,
    		@RequestParam(value="groupID", 			required=true) int groupId,
    		@RequestParam(value="questionId", 		required=true) int questionId) throws Exception
    {
    	
    	// Pseudo Code - question/delete
    	// Do checks for API, user and group and make sure all do exist
    	// Check that question with given Id exists
    		//if does not exist, throw exception "Question does not exist!"
    		//if exists, retrieve answers associated with question
    			// delete answers
    			// delete question
    			// check if answers and question deleted properly
    				//if no, throw exception
    				//if yes, return new QuestionStatusJson(false);

    	if (!APIKeyCheck.exists(apiKey)){
    		
    		throw new Exception("Failed to find your API key!");
    	}
    	
    	else 
    	{
    		// Set up your Database Tools
    		DatabaseTools myTools = new DatabaseTools();
    		
    		// Start by confirming / finding the user
    		boolean userFound = myTools.dataSearch("User", "id", userId, "password", password);
    		
    		if (!userFound) {
    			throw new Exception ("User not found");
    		}
    		
    		else
    		{
    			// next confirm the group exists
    			boolean groupFound = myTools.dataSearch("Group", "id", groupId);
    			
    			if (!groupFound)
    			{	// if group is not found, throw an exception
    				throw new Exception ("Group not found");
    			}
    			
    			//group exists
    			else
    			{
    				// Confirm that question with this id exists
    				boolean questionFound = myTools.dataSearch("Question", "id", questionId);
    				
    				if(!questionFound)
    				{	// if question is not found, throw an exception
    					throw new Exception ("Question not found!");
    				}
    				else // question is found
    				{
    					
    					//get the answers list attributed to a specified question
    					DatabasePro<Answer, Integer, Integer, String> myAnswerTools = new DatabasePro<>();
    					
    					List<Answer> answersList = myAnswerTools.getTypeList(Table.Answer, Column.questionId, questionId);
    					
    					
    					
    					if(!(answersList.size() > 0)) // no answers were found; this is a problem.
    					{
    						throw new Exception("There are no answers associated with this question.");
    					}
    					else 
    					{
    						for(int i = 0; i < answersList.size(); i++)
    						{
    							// Deleting the answer
    							myAnswerTools.dataRemoval(answersList.get(i));
    						}
    					}
    					
    					// setup tool to delete the question
    					DatabasePro<Question, Integer, Integer, String> myQuestionTools = new DatabasePro<>();
    					
    					//set up retrieval tools
       					ObjectRetrievalTools<Question> objTool = new ObjectRetrievalTools<Question>();
       					
       					//retrieve the Question
       					Question theQuestion = (Question)objTool.getObject(Table.Question, "id", questionId);
       					// Delete the question
    					myQuestionTools.dataRemoval(theQuestion);
    					
    					// Now check that the question is correctly stored in the database
	    			    questionFound = myTools.dataSearch("Question", "id", questionId);
	    			    // get the answers list again, to check if they were deleted; list size 0 = deleted
    					answersList = myAnswerTools.getTypeList(Table.Answer, Column.questionId, questionId);
    					
    					if(questionFound || answersList.size() > 0)
    					{
    						throw new Exception("The question was not deleted properly.");
    					}
    					else
    					{
    						// Return false, indicating the question does not exist anymore
    						return new QuestionStatusJson(false);
    					}
    				}  					
    			}    			   
    		}
    	}
    }
}