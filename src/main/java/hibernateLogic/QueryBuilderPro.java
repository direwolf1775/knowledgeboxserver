package hibernateLogic;

import hibernate.Column;
import hibernate.Table;

/**
 * These methods will return a string that can be 
 * used to query a database through hibernate. Each
 * column needs to be specified as String, Integer,
 * etc. to let the builder know what class to follow.
 * @author Vaclav Hnizda
 *
 * @param <Column1> This is the first column search type
 * @param <Column2> This is the second column search type
 * @param <Column3> This is the third column search type
 */
public class QueryBuilderPro<Column1,Column2,Column3> {
	public QueryBuilderPro(){ }
	
	/**
	 * This method will make hibernate query for the whole table. FYI!
	 * @param table
	 * @return
	 */
	public String string(Table table)
	{
		String query = "FROM " + table.toString();
		return query;
	}
	
	/**
	 * This method returns a string to do a query with a one column keyword search
	 * @param table
	 * @param column1Name
	 * @param column1
	 * @return
	 */
	public String string(Table table, Column column1Name, Column1 column1)
	{
		String query = "FROM " + table.toString() 
				   + " WHERE " + column1Name 
				   + " = \'"   + column1.toString() + "\'";
		return query;
	}
	
	/**
	 * This method returns a string to do a query with a two column keyword search
	 * @param table
	 * @param column1Name
	 * @param column1
	 * @param column2Name
	 * @param search2
	 * @return
	 */
	public String string(Table table, Column column1Name, Column1 column1
									, Column column2Name, Column2 search2)
	{
		String query = "FROM " + table.toString() 
				   + " WHERE " + column1Name  
				   + " = \'"   + column1.toString() + "\'" 
				   + " AND "   + column2Name 
				   + " = \'"   + search2.toString() + "\'";
		return query;
	}
	
	/**
	 * This method returns a string to do a query with a three column keyword search
	 * @param table
	 * @param column1Name
	 * @param column1
	 * @param column2Name
	 * @param column2
	 * @param column3Name
	 * @param column3
	 * @return
	 */
	public String string(Table table, Column column1Name, Column1 column1
									, Column column2Name, Column2 column2
									, Column column3Name, Column3 column3)
	{
		String query = "FROM " + table.toString() 
				   + " WHERE " + column1Name 
				   + " = \'"   + column1.toString()	+ "\'"
				   + " AND "   + column2Name 
				   + " = \'"   + column2.toString()	+ "\'"
				   + " AND "   + column3Name 
				   + " = \'"   + column3.toString()	+ "\'";
		return query;
	}
}
