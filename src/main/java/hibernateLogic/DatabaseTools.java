/**
 * @author Vaclav Hnizda
 * @author Anas A. Aljuwaiber
 *	Knowledge Box
 *	Project 2013-14
 *	SE491-591 - Software Engineering Studio
 */

package hibernateLogic;

import hibernate.Answer;
import hibernate.Group;
import hibernate.Question;
import hibernate.Score;
import hibernate.User;
import hibernate.UserRoleGroup;

import java.util.Date;
import java.util.List;

import org.hibernate.Session;

public class DatabaseTools {
		
	/*************** SEARCH METHODS *****************************/
	/*															*/
	/*		This means these methods are						*/
	/*		non-destructive. No data is edited.					*/
	/*															*/
	/************************************************************/
	
	// For searching an item by id
	public boolean dataSearch(String tableName, String columnName, int id)
	{
		// Pre-create string used for query search
		String hql = DataQueryBuilder.searchString(tableName, columnName, id);
		
		// Open a new session through Hibernate
		Session session = MyHibernateFactory.newSession();
		session.beginTransaction();
		
		// Run a query to confirm they are in the database
		List result = session.createQuery(hql).list();
		
		// Commit & Close the session when you are done with the database!
		session.getTransaction().commit();
		session.close();
		if (result.size() > 0){
			return true;
		}
		else
			return false;
}

	/**
	 * Search through a table with one specified variable
	 * @param tableName
	 * @param columnName
	 * @param word
	 * @return True if data found, false if nothing found.
	 */
	public boolean dataSearch(String tableName, String columnName, String word){
		
		// Pre-create string used for query search
		String hql = DataQueryBuilder.searchString(tableName, columnName, word);

		// Open a new session through Hibernate
		Session session = MyHibernateFactory.newSession();
		session.beginTransaction();
		
   		// Run a query to confirm they are in the database
		List result = session.createQuery(hql).list();
		
		// Commit & Close the session when you are done with the database!
		session.getTransaction().commit();
		session.close();
		if (result.size() > 0){
			return true;
		}
		else
			return false;
	}
	

	/**
	 * Search through a table with 2 specified columns
	 * @param tableName
	 * @param columnName
	 * @param word
	 * @param columnName2
	 * @param word2
	 * @return True if the query returned a result
	 */
	public boolean dataSearch(String tableName, String columnName, String word, String columnName2, String word2){
		
		// Pre-create string used for query search
		String hql = DataQueryBuilder.searchString(tableName, columnName, word, columnName2, word2);

		// Open a new session through Hibernate
		Session session = MyHibernateFactory.newSession();
		session.beginTransaction();
		
   		// Run a query to confirm they are in the database
		List result = session.createQuery(hql).list();
		
		// Commit & Close the session when you are done with the database!
		session.getTransaction().commit();
		session.close();
		if (result.size() > 0){
			return true;
		}
		else
			return false;
	}

	
	public boolean dataSearch(String tableName, String columnName, int id, String columnName2, String word2){
		
		// Pre-create string used for query search
		String hql = DataQueryBuilder.searchString(tableName, columnName, id, columnName2, word2);
		
		// Open a new session through Hibernate
		Session session = MyHibernateFactory.newSession();
		session.beginTransaction();
		
		// Run a query to confirm they are in the database
		List result = session.createQuery(hql).list();
		
		// Commit & Close the session when you are done with the database!
		session.getTransaction().commit();
		session.close();
		if (result.size() > 0){
			return true;
		}
		else
			return false;
	}
	
	
	/**
	 * This functions is for searching groups by the date of creation ?
	 * @param tableName
	 * @param columnName
	 * @param word
	 * @param columnName2
	 * @param word2
	 * @return True if the query returned a result
	 */
	public boolean dataSearch(String tableName, String columnName, String word, String columnName2, Date word2)
	{	
		// Pre-create string used for query search
		String hql = DataQueryBuilder.searchString(tableName, columnName, word, columnName2, word2);

		// Open a new session through Hibernate
		Session session = MyHibernateFactory.newSession();
		session.beginTransaction();
		
   		// Run a query to confirm they are in the database
		List result = session.createQuery(hql).list();
		
		// Commit & Close the session when you are done with the database!
		session.getTransaction().commit();
		session.close();
		if (result.size() > 0){
			return true;
		}
		else
			return false;
	}
	
	/**
	 * This search returns an instance of the object if found otherwise error out
	 * @param email
	 * @param password
	 * @return instance of the User object from the database
	 * @throws Exception if user was not found in the database
	 */
	public User getUser(String email, String password) throws Exception{
		
		// Pre-create string used for query search
		String hql = DataQueryBuilder.searchString("User", "email", email, "password", password);

		// Open a new session through Hibernate
		Session session = MyHibernateFactory.newSession();
		session.beginTransaction();
		
   		// Run a query to find the requested user
		List result = session.createQuery(hql).list();
		
		// Commit & Close the session when you are done with the database!
		session.getTransaction().commit();
		session.close();
		
		if (result.size() ==0){
			throw new Exception("Could not get User object. User is not in the database!");
		}
		else{
			return (User)result.get(0);
		}
	}
	
	/**
	 * This search returns a list of Users found.
	 * @return
	 * @throws Exception
	 */
	public List<User> getUserList() throws Exception{
		
		// Pre-create string used for query search
		String hql = DataQueryBuilder.searchString("User");
		
		// Open a new session through Hibernate
		Session session = MyHibernateFactory.newSession();
		session.beginTransaction();
		
		// Run a query to find the requested user
		List<User> result = (List<User>)session.createQuery(hql).list();
		
		// Commit & Close the session when you are done with the database!
		session.getTransaction().commit();
		session.close();
		
		if (result.size() ==0){
			throw new Exception("Could not get User List. Database is empty or offline!");
		}
		else{
			return result;
		}
	}
	
	/**
	 * This search returns a list of all Groups found in the database.
	 * @return
	 * @throws Exception
	 */
	public List<Group> getGroupList() throws Exception{
		
		// Pre-create string used for query search
		String hql = DataQueryBuilder.searchString("Group");
		
		// Open a new session through Hibernate
		Session session = MyHibernateFactory.newSession();
		session.beginTransaction();
		
		// Run a query to find the requested user
		List<Group> result = (List<Group>)session.createQuery(hql).list();
		
		// Commit & Close the session when you are done with the database!
		session.getTransaction().commit();
		session.close();
		
		if (result.size() ==0){
			throw new Exception("Could not get User List. Database is empty or offline!");
		}
		else{
			return result;
		}
	}
	
	/************ DATA EDITING METHODS **************************/
	/*															*/
	/*		This means these methods can						*/
	/*		alter the existing data!							*/
	/*															*/
	/************************************************************/
	
	/**
	 * Add a User entry to the database
	 * @param newUser
	 */
	public int dataEntry(User newUser){
		
		// Open a new session
		Session session = MyHibernateFactory.newSession();
		session.beginTransaction();
		
		// Add the user
		session.save(newUser);
		
		// Commit & Close the session when you are done with the database!
		session.getTransaction().commit();
		session.close();
		
		return newUser.getId();
		
	}
	
	/**
	 * Delete a User entry to the database
	 * @param deleteUser
	 */
	public void dataRemoval(User deleteUser){
		
		// Open a new session
		Session session = MyHibernateFactory.newSession();
    	session.beginTransaction();
    	
    	// Set up the deletion of the User
    	session.delete(deleteUser);
    	
    	// Commit & Close the session when you are done with the database!
		session.getTransaction().commit();
		session.close();

	}
	
	/**
	 * Update a User entry to the database
	 * @param udateUser
	 */
	public void dataUpdate(User udateUser){
		
		// Open a new session
		Session session = MyHibernateFactory.newSession();
    	session.beginTransaction();
    	
    	// Set up the update of the User
    	session.update(udateUser);
    	
    	// Commit & Close the session when you are done with the database!
		session.getTransaction().commit();
		session.close();

	}
	
	/**
	 * Add a UserRoleGroup entry to the database
	 * @param newUserRoleGroup
	 */
	public int dataEntry(UserRoleGroup newUserRoleGroup){
		
		// Open a new session
		Session session = MyHibernateFactory.newSession();
		session.beginTransaction();
		
		// Add the UserRoleGroup
		session.save(newUserRoleGroup);
		
		// Commit & Close the session when you are done with the database!
		session.getTransaction().commit();
		session.close();
		
		return newUserRoleGroup.getId();
	}
	
	/**
	 * Delete a UserRoleGroup entry to the database
	 * @param deleteUserRoleGroup
	 */
	public void dataRemoval(UserRoleGroup deleteUserRoleGroup){
		
		// Open a new session
		Session session = MyHibernateFactory.newSession();
    	session.beginTransaction();
    	
    	// Set up the deletion of the UserRoleGroup
    	session.delete(deleteUserRoleGroup);
    	
    	// Commit & Close the session when you are done with the database!
		session.getTransaction().commit();
		session.close();
	}
	
	/**
	 * Update a UserRoleGroup entry to the database
	 * @param udateUserRoleGroup
	 */
	public void dataUpdate(UserRoleGroup udateUserRoleGroup){
		
		// Open a new session
		Session session = MyHibernateFactory.newSession();
    	session.beginTransaction();
    	
    	// Set up the update of the UserRoleGroup
    	session.update(udateUserRoleGroup);
    	
    	// Commit & Close the session when you are done with the database!
		session.getTransaction().commit();
		session.close();
	}
	
	/**
	 * Add a Group entry to the database
	 * @param newGroup
	 */
	public int dataEntry(Group newGroup){
		
		// Open a new session
		Session session = MyHibernateFactory.newSession();
		session.beginTransaction();
		
		// Add the group
		session.save(newGroup);
		
		// Commit & Close the session when you are done with the database!
		session.getTransaction().commit();
		session.close();	
		
		return newGroup.getId();
	}
	
	/**
	 * Delete a Group entry to the database
	 * @param deleteGroup
	 */
	public void dataRemoval(Group deleteGroup){
		
		// Open a new session
		Session session = MyHibernateFactory.newSession();
    	session.beginTransaction();
    	
    	// Set up the deletion of the group
    	session.delete(deleteGroup);
    	
    	// Commit & Close the session when you are done with the database!
		session.getTransaction().commit();
		session.close();
	}
	
	/**
	 * Update a Group entry to the database
	 * @param udateGroup
	 */
	public void dataUpdate(Group udateGroup){
		
		// Open a new session
		Session session = MyHibernateFactory.newSession();
    	session.beginTransaction();
    	
    	// Set up the update of the Group
    	session.update(udateGroup);
    	
    	// Commit & Close the session when you are done with the database!
		session.getTransaction().commit();
		session.close();
	}
	
	/**
	 * Add a Question entry to the database
	 * @param newQuestion
	 */
	public int dataEntry(Question newQuestion){
		
		// Open a new session
		Session session = MyHibernateFactory.newSession();
		session.beginTransaction();
		
		// Add the question
		session.save(newQuestion);
		
		// Commit & Close the session when you are done with the database!
		session.getTransaction().commit();
		session.close();
		
		return newQuestion.getId();
	}
	
	/**
	 * Delete a Question entry to the database
	 * @param deleteQuestion
	 */
	public void dataRemoval(Question deleteQuestion){
		
		// Open a new session
		Session session = MyHibernateFactory.newSession();
    	session.beginTransaction();
    	
    	// Set up the deletion of the question
    	session.delete(deleteQuestion);
    	
    	// Commit & Close the session when you are done with the database!
		session.getTransaction().commit();
		session.close();

	}

	/**
	 * Update a Question entry to the database
	 * @param udateQuestion
	 */
	public void dataUpdate(Question udateQuestion){
		
		// Open a new session
		Session session = MyHibernateFactory.newSession();
    	session.beginTransaction();
    	
    	// Set up the update of the Question
    	session.update(udateQuestion);
    	
    	// Commit & Close the session when you are done with the database!
		session.getTransaction().commit();
		session.close();
	}
	
	/**
	 * Add a Answer entry to the database
	 * @param newAnswer
	 */
	public int dataEntry(Answer newAnswer){
		
		// Open a new session
		Session session = MyHibernateFactory.newSession();
		session.beginTransaction();
		
		// Add the answer
		session.save(newAnswer);
		
		// Commit & Close the session when you are done with the database!
		session.getTransaction().commit();
		session.close();
		
		return newAnswer.getId();
	}
	
	/**
	 * Delete a Answer entry to the database
	 * @param deleteAnswer
	 */
	public void dataRemoval(Answer deleteAnswer){
		
		// Open a new session
		Session session = MyHibernateFactory.newSession();
    	session.beginTransaction();
    	
    	// Set up the deletion of the answer
    	session.delete(deleteAnswer);
    	
    	// Commit & Close the session when you are done with the database!
		session.getTransaction().commit();
		session.close();

	}

	/**
	 * Update a Answer entry to the database
	 * @param udateAnswer
	 */
	public void dataUpdate(Answer udateAnswer){
		
		// Open a new session
		Session session = MyHibernateFactory.newSession();
    	session.beginTransaction();
    	
    	// Set up the update of the Answer
    	session.update(udateAnswer);
    	
    	// Commit & Close the session when you are done with the database!
		session.getTransaction().commit();
		session.close();
	}
	
	/**
	 * Add a Score entry to the database
	 * @param newScore
	 */
	public int dataEntry(Score newScore){
		
		// Open a new session
		Session session = MyHibernateFactory.newSession();
		session.beginTransaction();
		
		// Add the Score
		session.save(newScore);
		
		// Commit & Close the session when you are done with the database!
		session.getTransaction().commit();
		session.close();
		
		return newScore.getId();
	}
	
	/**
	 * Delete a Score entry to the database
	 * @param deleteScore
	 */
	public void dataRemoval(Score deleteScore){
		
		// Open a new session
		Session session = MyHibernateFactory.newSession();
    	session.beginTransaction();
    	
    	// Set up the deletion of the answer
    	session.delete(deleteScore);
    	
    	// Commit & Close the session when you are done with the database!
		session.getTransaction().commit();
		session.close();

	}

	/**
	 * Update a Score entry to the database
	 * @param udateScore
	 */
	public void dataUpdate(Score udateScore){
		
		// Open a new session
		Session session = MyHibernateFactory.newSession();
    	session.beginTransaction();
    	
    	// Set up the update of the Score
    	session.update(udateScore);
    	
    	// Commit & Close the session when you are done with the database!
		session.getTransaction().commit();
		session.close();
	}
}