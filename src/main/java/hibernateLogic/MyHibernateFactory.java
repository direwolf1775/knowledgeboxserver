/**
  * @author Vaclav Hnizda
  * Knowledge Box
  *	Project 2013-14
  *	SE491-591 - Software Engineering Studio
  * 
  * Singleton Class that manages the SessionFactory
  */

package hibernateLogic;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;


public class MyHibernateFactory {
	
	private static SessionFactory sessionFactory = new Configuration()
												  .configure() // configures settings from hibernate.cfg.xml
												  .buildSessionFactory();
	
	private MyHibernateFactory() { }
	
	public static SessionFactory getFactory(){
		return sessionFactory;
	}
	public static Session newSession() {
		return sessionFactory.openSession();
	}
	
	
	
	
}

//Example:
//http://www.tutorialspoint.com/java/java_using_singleton.htm
//public class Singleton {
//
//	   private static Singleton singleton = new Singleton( );
//	   
//	   /* A private Constructor prevents any other 
//	    * class from instantiating.
//	    */
//	   private Singleton(){ }
//	   
//	   /* Static 'instance' method */
//	   public static Singleton getInstance( ) {
//	      return singleton;
//	   }
//	   /* Other methods protected by singleton-ness */
//	   protected static void demoMethod( ) {
//	      System.out.println("demoMethod for singleton"); 
//	   }
//	}
