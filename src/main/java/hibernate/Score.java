package hibernate;

import java.util.Date;

/**
 * 
 * @author Paul Pelafas
 *This class is used by Hibernate to communicate with the database
 */
public class Score implements HibernateTable {
	
	private int id, userId, groupId, rightAnswers, wrongAnswers,
				totalAnswers, score;
	private Date dateTaken;

	/**
	 * Empty constructor
	 */
	public Score(){}
	
	/**
	 * Method calls mutator methods to set fields
	 * @param userId is the parameter to be set to the local variable
	 * @param groupId is the parameter to be set to the local variable
	 * @param rightAnswers is the parameter to be set to the local variable
	 * @param wrongAnswers is the parameter to be set to the local variable
	 * @param totalAnswers is the parameter to be set to the local variable
	 * @param score is the parameter to be set to the local variable
	 */
	public Score(int userId, int groupId, int rightAnswers, int wrongAnswers, 
			int totalAnswers, int score){
		setUserId(userId);
		setGroupId(groupId);
		setRightAnswers(rightAnswers);
		setWrongAnswers(wrongAnswers);
		setTotalAnswers(totalAnswers);
		setScore(score);
	}
	
	/**
	 * Method to return id
	 * @return is the id
	 */
	public int getId(){
		return id;
	}
	
	/**
	 * Method to set the id
	 * @param id parameter to be set to local variable
	 */
	private void setId(int id){
		this.id = id;
	}
	
	/**
	 * Method to return userId
	 * @return is the userId
	 */
	public int getUserId(){
		return userId;
	}
	
	/**
	 * Method to set the userId
	 * @param userId is the parameter to be set to the local variable
	 */
	public void setUserId(int userId){
		this.userId = userId;
	}
	
	/**
	 * Method to return groupId
	 * @return is the groupId
	 */
	public int getGroupId(){
		return groupId;
	}
	
	/**
	 * Method to set the groupId
	 * @param groupId is the parameter to be set to the local variable
	 */
	public void setGroupId(int groupId){
		this.groupId = groupId;
	}
	
	/**
	 * Method to return rightAnswers
	 * @return is the rightAnswers
	 */
	public int getRightAnswers(){
		return rightAnswers;
	}
	
	/**
	 * Method to set rightAnswers
	 * @param rightAnswers is the parameter to be set to the local variable
	 */
	public void setRightAnswers(int rightAnswers){
		this.rightAnswers = rightAnswers;
	}
	
	/**
	 * Method to return wrongAnswers
	 * @return is the wrongAnswers
	 */
	public int getWrongAnswers(){
		return wrongAnswers;
	}
	
	/**
	 * Method sets the wrongAnswers
	 * @param wrongAnswers is the parameter to be set to the local variable
	 */
	public void setWrongAnswers(int wrongAnswers){
		this.wrongAnswers = wrongAnswers;
	}
	
	/**
	 * Method gets the totalAnswers
	 * @return is totalAnswers
	 */
	public int getTotalAnswers(){
		return totalAnswers;
	}
	
	/**
	 * Method sets the totalAnswers
	 * @param totalAnswers is the parameter to be set to the local variable
	 */
	public void setTotalAnswers(int totalAnswers){
		this.totalAnswers = totalAnswers;
	}
	
	/**
	 * Method returns score
	 * @return is score
	 */
	public int getScore(){
		return score;
	}
	
	/**
	 * Method sets score
	 * @param score is the parameter to be set to the local variable
	 */
	public void setScore(int score){
		this.score = score;
	}

	/**
	 * @return the dateTaken
	 */
	public Date getDateTaken() {
		return dateTaken;
	}

	/**
	 * @param dateTaken the dateTaken to set
	 */
	public void setDateTaken(Date dateTaken) {
		this.dateTaken = dateTaken;
	}
}
