package hibernate;

import java.util.Date;

/**
 * 
 * @author Paul Pelafas
 * Class is used by Hibernate to communicate with the database
 */
public class UserRoleGroup implements HibernateTable {
	
	private int id, userId, groupId, roleId;
	
	private Date createDate, modifyDate;
	
	/**
	 * Empty constructor
	 */
	public UserRoleGroup(){}
	
	/**
	 * Constructor calls the mutators to set the local fields
	 * @param userId
	 * @param groupId
	 * @param roleId
	 * @param createDate
	 */
	public UserRoleGroup(int userId, int groupId, int roleId, Date createDate){
		setUserId(userId);
		setGroupId(groupId);
		setRoleId(roleId);
		setCreateDate(createDate);
	}
	
	/**
	 * Method gets the id
	 * @return is the id
	 */
	public int getId(){
		return id;
	}

	/**
	 * Method sets the id 
	 * @param id is the parameter to be set to the local field
	 */
	private void setId(int id){
		this.id = id;
	}
	
	/**
	 * Method returns userId
	 * @return is userId
	 */
	public int getUserId(){
		return userId;
	}
	
	/**
	 * Method is used to set the userId
	 * @param userId is the parameter to set to the local field
	 */
	public void setUserId(int userId){
		this.userId = userId;
	}
	
	/**
	 * Method gets the groupId
	 * @return is the groupId
	 */
	public int getGroupId(){
		return groupId;
	}
	
	/**
	 * Method is used to set the groupId
	 * @param topicID is the parameter to be set to the local variable
	 */
	public void setGroupId(int groupId){
		this.groupId = groupId;
	}
	
	/**
	 * Method returns the roleId
	 * @return is the roleId
	 */
	public int getRoleId(){
		return roleId;
	}
	
	/**
	 * Method sets the roleId
	 * @param roleId is the parameter to set to the local field
	 */
	public void setRoleId(int roleId){
		this.roleId = roleId;
	}
	
	/**
	 * @return the createDate
	 */
	public Date getCreateDate() {
		return createDate;
	}

	/**
	 * @param createDate the createDate to set
	 */
	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	/**
	 * @return the modifyDate
	 */
	public Date getModifyDate() {
		return modifyDate;
	}

	/**
	 * @param modifyDate the modifyDate to set
	 */
	public void setModifyDate(Date modifyDate) {
		this.modifyDate = modifyDate;
	}
}
