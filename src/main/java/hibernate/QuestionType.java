package hibernate;

import java.util.Date;

/**
 * 
 * @author Paul Pelafas
 * This class is used by Hibernate to communicate with the database
 */
public class QuestionType implements HibernateTable {
	
	private int id;
	
	private String questionTypeName;
	
	private Date createDate, modifyDate;
	
	/**
	 * Empty constructor
	 */
	public QuestionType(){}
	
	/**
	 * Constructor sets the questionTypeName
	 * @param questionTypeName
	 */
	public QuestionType(String questionTypeName, Date createDate){
		setQuestionTypeName(questionTypeName);
		setCreateDate(createDate);
	}
	
	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	private void setId(int id) {
		this.id = id;
	}
	
	/**
	 * Method gets the questionTypeName
	 * @return is the questionTypeName
	 */
	public String getQuestionTypeName(){
		return questionTypeName;
	}

	/**
	 * Method sets the questionTypeName
	 * @param questionTypeName is the parameter to be set to the local field
	 */
	public void setQuestionTypeName(String questionTypeName){
		this.questionTypeName = questionTypeName;
	}
	
	/**
	 * @return the createDate
	 */
	public Date getCreateDate() {
		return createDate;
	}

	/**
	 * @param createDate the createDate to set
	 */
	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	/**
	 * @return the modifyDate
	 */
	public Date getModifyDate() {
		return modifyDate;
	}

	/**
	 * @param modifyDate the modifyDate to set
	 */
	public void setModifyDate(Date modifyDate) {
		this.modifyDate = modifyDate;
	}
}
