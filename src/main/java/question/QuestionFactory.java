/**
 *  @author Vaclav Hnizda
 *  @author Paul Pelafas
 *	Knowledge Box
 *	Project 2013-14
 *	SE491-591 - Software Engineering Studio
 */

package question;

import java.util.Date;

import hibernate.Answer;
import hibernate.Question;
import user.UserQuestionJson;

public class QuestionFactory {
	
	private QuestionFactory(){}
	
	/**
	 * Create an empty array of UserQuestionJson 
	 * @param size
	 * @return
	 */
	public static UserQuestionJson[] createUserQuestionJson(int size){
		return new UserQuestionJson[size];
		
	}
	
	/**
	 * Create a UserQuestionJson object with all the fields extracted from a given Question
	 * @param myQuestion
	 * @return
	 */
	public static UserQuestionJson createUserQuestionJson(Question myQuestion){
		
		//TO DO must create a new object and return it with all the info taken from Question
		return null;
		
	}
	
	/**
	 * This method is used to create a new multiple choice question object
	 * @param groupId
	 * @param questionText
	 * @return this is the returned multiple choice object
	 */
	public static Question createQuestionMC(int userId, int groupId, String questionText){

		Date createDate = new Date();
		
		Question questionMC = new Question(questionText);
		questionMC.setUserId(userId);
		questionMC.setGroupId(groupId);
		questionMC.setQuestionTypeId(1);
		questionMC.setCreateDate(createDate);
		
		return questionMC;
	}
	
	/**
	 * This method is used to create a new true/false question object
	 * @param userId
	 * @param groupId
	 * @param questionText
	 * @return is the return true/false question
	 */
	public static Question createQuestionTF(int userId, int groupId, String questionText){

		Date createDate = new Date();
		
		Question questionTF = new Question(questionText);
		questionTF.setUserId(userId);
		questionTF.setGroupId(groupId);
		questionTF.setQuestionTypeId(2);
		questionTF.setCreateDate(createDate);
		
		return questionTF;
	}
	
	/**
	 * This method is used to create an answer object
	 * @param answerText
	 * @param sortOrder
	 * @param questionId
	 * @param isCorrect
	 * @return is the returned answer object
	 */
	public static Answer createAnswer(String answerText, int sortOrder, int questionId,  boolean isCorrect){
		
		Answer answer = new Answer(answerText);
		answer.setSortOrder(sortOrder);
		answer.setQuestionId(questionId);
		answer.setIsCorrect(isCorrect);
		
		return answer;
	}
	
	/**
	 * This method is used to create QuestionDetailsJson[] of a specific size
	 * @param size
	 * @return is the returned empty array
	 */
	public static QuestionDetailsJson[]  createQuestionDetailsJson(int size) {	
		return new QuestionDetailsJson[size];
	}
	
	/**
	 * This method is used to create a new QuestionDetailsJson object
	 * @param myQuestion
	 * @return is the returned QuestionDetailsJson object
	 */
	public static QuestionDetailsJson createQuestionDetailsJson(Question myQuestion) {
		QuestionDetailsJson questionInfo = new QuestionDetailsJson();
		
		// Copying the values in the Question object passed into the function to the return Json object
		questionInfo.setCreateDate(myQuestion.getCreateDate());
		questionInfo.setGroupId(myQuestion.getGroupId());
		questionInfo.setId(myQuestion.getId());
		questionInfo.setQuestionText(myQuestion.getQuestionText());
		questionInfo.setQuestionTypeId(myQuestion.getQuestionTypeId());
		return questionInfo;
	}
	
	/**
	 * This method is used to create a new QuestionDetailsJson object
	 * @param myQuestion
	 * @return is the returned QuestionDetailsJson object
	 */
	public static QuestionDetailsJson createQuestionDetailsJson(Question myQuestion, String groupTitle) {
		QuestionDetailsJson questionInfo = new QuestionDetailsJson();
		
		// Copying the values in the Question object passed into the function to the return Json object
		questionInfo.setCreateDate(myQuestion.getCreateDate());
		questionInfo.setGroupId(myQuestion.getGroupId());
		questionInfo.setId(myQuestion.getId());
		questionInfo.setQuestionText(myQuestion.getQuestionText());
		questionInfo.setQuestionTypeId(myQuestion.getQuestionTypeId());
		questionInfo.setGroupTitle(groupTitle);
		return questionInfo;
	}

}
