/**
 * 	@author Sabrina Guillaume
 *  @author Vaclav Hnizda
 *	Knowledge Box
 *	Project 2013-14
 *	SE491-591 - Software Engineering Studio
 *
 *  Hibernate Testing Grounds!
 */
package hibernate;

import java.util.Date;
import java.util.List;

import junit.framework.TestCase;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class HibernateTest extends TestCase {
	private SessionFactory sessionFactory;

	@SuppressWarnings("deprecation")
	@Override
	protected void setUp() throws Exception {
		// A SessionFactory is set up once for an application
        sessionFactory = new Configuration()
                .configure() // configures settings from hibernate.cfg.xml
                .buildSessionFactory();
	}

	@Override
	protected void tearDown() throws Exception {
		if ( sessionFactory != null ) {
			sessionFactory.close();
		}
	}


	public void testQuestionTable01(){
		// create a new session connection to database
		Session session = sessionFactory.openSession();
		
		// begin the transaction, connection
		session.beginTransaction();
		
		// add new entries
		session.save(new Question("What is 5 times 11?") );
		session.save(new Question("Why did the chicken cross the road?") );
		// close the session
		session.getTransaction().commit();
		session.close();
		
		// now to pull the events from the database
		session = sessionFactory.openSession();
		session.beginTransaction();
		List result = session.createQuery( "from Question" ).list();
		for ( Question user : (List<Question>) result ) {
			System.out.println( "Question: " + user.getQuestionText() );
		}
		session.getTransaction().commit();
		session.close();
		
	}
	
	public void testAnswerTable01(){
		// create a new session connection to database
		Session session = sessionFactory.openSession();
		
		// begin the transaction, connection
		session.beginTransaction();
		
		// add new entries
		session.save(new Answer("55") );
		session.save(new Answer("To get to the other side") );
		// close the session
		session.getTransaction().commit();
		session.close();
		
		// now to pull the events from the database
		session = sessionFactory.openSession();
		session.beginTransaction();
		List result = session.createQuery( "from Answer" ).list();
		for ( Answer user : (List<Answer>) result ) {
			System.out.println( "Answer: " + user.getAnswerText() );
		}
		session.getTransaction().commit();
		session.close();

	}
	
	public void testUserTable01(){
		// create a new session connection to database
		Session session = sessionFactory.openSession();
		
		// begin the transaction, connection
		session.beginTransaction();
		Date now = new Date();
		
		// add new entries
		session.save(new User("sabrina@gmail.com","password","Sabrina","G",now) );
		session.save(new User("vaclav@gmail.com","password","Vaclav","H",now) );
		session.save(new User("charles@gmail.com","password","Charles","K",now) );
		session.save(new User("paul@gmail.com","password","Paul","P",now) );
		session.save(new User("anas@gmail.com","password","Anas","A",now) );
		
		// close the session
		session.getTransaction().commit();
		session.close();
		
		// now to pull the events from the database
		session = sessionFactory.openSession();
		session.beginTransaction();
		List result = session.createQuery( "from User" ).list();
		for ( User user : (List<User>) result ) {
			System.out.println( "Email: " + user.getEmail() );
			System.out.println( "Password: " + user.getPassword() );
			System.out.println( "First Name: " + user.getFirstName() );
			System.out.println( "Last Name: " + user.getLastName() );
		}
		session.getTransaction().commit();
		session.close();

	}
	
	public void test6GroupTable(){
		// create a new session connection to database
		Session session = sessionFactory.openSession();
		
		// begin the transaction, connection
		session.beginTransaction();
		Date now = new Date();
		
		// add new entries
		session.save(new Group("Oragami","The essentials of oragami",0));
		session.save(new Group("Physics","Physics 101 - DePaul University",0));
		session.save(new Group("Chemistry","Intro to Chemistry",0));
		session.save(new Group("Chess Club","Top strategies for mastering chess",0));
		session.save(new Group("Motorcycle Repair","Best Techniques for repairing motorcycles",0));
		
		// close the session
		session.getTransaction().commit();
		session.close();
		
		// now to pull the events from the database
		session = sessionFactory.openSession();
		session.beginTransaction();
		List result = session.createQuery( "from Group" ).list();
		for ( Group topic : (List<Group>) result ) {
			System.out.println( "OrgId: " + topic.getOrgId());
			System.out.println( "Group Title: " + topic.getGroupTitle() );
			System.out.println( "Group Description: " + topic.getGroupDesc() );
			
			
		}
		session.getTransaction().commit();
		session.close();

	}
	
	public void test7OrgTable(){
		// create a new session connection to database
		Session session = sessionFactory.openSession();
		
		// begin the transaction, connection
		session.beginTransaction();
		Date now = new Date();
		
		// add new entries
		session.save(new Org("DePaul University",now));
		session.save(new Org("Chester Library",now));
		session.save(new Org("LA Motor Trends",now)); 
		
		// close the session
		session.getTransaction().commit();
		session.close();
		
		// now to pull the events from the database
		session = sessionFactory.openSession();
		session.beginTransaction();
		List result = session.createQuery( "from Org" ).list();
		for ( Org org : (List<Org>) result ) {
			System.out.println( "Org Name: " + org.getOrgName());
		}
		session.getTransaction().commit();
		session.close();

	}
	
	public void test8QuestionTypeDimension(){
		// create a new session connection to database
		Session session = sessionFactory.openSession();
		
		// begin the transaction, connection
		session.beginTransaction();
		Date now = new Date();
		
		// add new entries
		session.save(new QuestionType("Multiple Choice",now));
		session.save(new QuestionType("True/False",now));
		session.save(new QuestionType("Yes/No",now));
		
		// close the session
		session.getTransaction().commit();
		session.close();
		
		// now to pull the events from the database
		session = sessionFactory.openSession();
		session.beginTransaction();
		List result = session.createQuery( "from QuestionType" ).list();
		for ( QuestionType qType : (List<QuestionType>) result ) {
			System.out.println( "Question Type Name: " + qType.getQuestionTypeName());
		}
		session.getTransaction().commit();
		session.close();

	}
	
	public void test9ScoreTable(){
		// create a new session connection to database
		Session session = sessionFactory.openSession();
		
		// begin the transaction, connection
		session.beginTransaction();
		Date now = new Date();
		
		// add new entries
		session.save(new Score(1,1,5,0,5,100));
		session.save(new Score(2,1,5,0,5,100));
		session.save(new Score(3,1,5,0,5,100));
		session.save(new Score(3,2,5,0,5,100));
		
		// close the session
		session.getTransaction().commit();
		session.close();
		
		// now to pull the events from the database
		session = sessionFactory.openSession();
		session.beginTransaction();
		List result = session.createQuery( "from Score" ).list();
		for ( Score score : (List<Score>) result ) {
			System.out.println( "User Id: " + score.getUserId());
			System.out.println( "Group Id: " + score.getGroupId());
			System.out.println( "Right Answers: " + score.getRightAnswers());
			System.out.println( "Wrong Answers: " + score.getWrongAnswers());
			System.out.println( "Total Answers: " + score.getTotalAnswers());
			System.out.println( "Score: " + score.getScore());
		}
		session.getTransaction().commit();
		session.close();

	}
	public void test10UserRoleDimension(){
		// create a new session connection to database
		Session session = sessionFactory.openSession();
		
		// begin the transaction, connection
		session.beginTransaction();
		Date now = new Date();
		
		// add new entries
		session.save(new UserRoles("Administrator",now));
		session.save(new UserRoles("Member",now));
		session.save(new UserRoles("Leader",now));

		
		// close the session
		session.getTransaction().commit();
		session.close();
		
		// now to pull the events from the database
		session = sessionFactory.openSession();
		session.beginTransaction();
		List result = session.createQuery( "from UserRoles" ).list();
		for ( UserRoles userRoles : (List<UserRoles>) result ) {
			System.out.println( "Role Name: " + userRoles.getRoleName());
			System.out.println( "Create Date: " + userRoles.getCreateDate());
		}
		session.getTransaction().commit();
		session.close();

	}
	
	public void test11UserRoleGroupTable(){
		// create a new session connection to database
		Session session = sessionFactory.openSession();
		
		// begin the transaction, connection
		session.beginTransaction();
		Date now = new Date();
		
		// add new entries
		session.save(new UserRoleGroup(1,2,2,now));
		session.save(new UserRoleGroup(1,2,2,now));
		session.save(new UserRoleGroup(2,1,1,now));
		session.save(new UserRoleGroup(3,2,2,now));
		session.save(new UserRoleGroup(4,2,2,now));

		
		// close the session
		session.getTransaction().commit();
		session.close();
		
		// now to pull the events from the database
		session = sessionFactory.openSession();
		session.beginTransaction();
		List result = session.createQuery( "from UserRoleGroup" ).list();
		for ( UserRoleGroup userRoleGroup : (List<UserRoleGroup>) result ) {
			System.out.println( "User Id: " + userRoleGroup.getUserId());
			System.out.println( "Group Id: " + userRoleGroup.getGroupId());
			System.out.println( "Role Id: " + userRoleGroup.getRoleId());
			System.out.println( "Create Date: " + userRoleGroup.getCreateDate());
		}
		session.getTransaction().commit();
		session.close();

	}

}
