Follow these instructions for this test to run successfully

1. Have MySQL running in the background (localhost)
2. Create a schema named "test_schema" as specified in the hibernate.cfg.xml
3. Then just run the Junit test "NativeApiIllustrationTest"
	a. you will see the table events created
	b. you will receive a view that the entries were saved and retrieved