![Box image](https://bitbucket.org/VaclavH/knowledgeboxserver/wiki/rubics_cube.png "Logo")

#Knowledge Box Server

This is a program that allows group collaboration
in an online environment. The system connects to a
MySQL database on the back end and pushes public 
APIs to the client..